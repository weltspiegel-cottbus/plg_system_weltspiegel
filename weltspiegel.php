<?php

defined('_JEXEC') or die;

class PlgSystemWeltspiegel extends JPlugin
{
  public function onAfterInitialise()
  {
    // Register Weltspiegel Lib Autoloader
    require JPATH_LIBRARIES . '/weltspiegel/vendor/autoload.php';
  }
}
