// Taskrunner: Gulp (https://gulpjs.com/)
const { src, dest, series } = require('gulp');

// Gulp Plugins
const zip  = require('gulp-zip');

// Tasks

function packagePlugin() {
  return src(['weltspiegel.*', 'index.html', 'README.md', 'LICENSE'])
    .pipe(zip('plg_system_weltspiegel.zip'))
    .pipe(dest('.'));
}

exports.default = series(packagePlugin);
